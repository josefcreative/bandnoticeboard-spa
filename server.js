const express = require('express');
const app = express();
const path = require('path')

app.use(require('prerender-node').set('prerenderToken', 'dNoQGN6JaGxFf2STvO3X'));
app.use('/', express.static(path.join(__dirname, '')));

app.listen(4005);