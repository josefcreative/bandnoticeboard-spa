import { BandNoticeboardPage } from './app.po';

describe('band-noticeboard App', () => {
  let page: BandNoticeboardPage;

  beforeEach(() => {
    page = new BandNoticeboardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
