import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { User } from '../models/user.models';
import { PostService } from '../services/post.service';
import { Post } from '../models/post.models';
import { HttpService } from '../services/http.service';
import { AuthService } from '../services/auth.service';
import { UtilityService } from '../services/utility.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  user: User = null;
  posts: Post;
  data: any = {};
  messageViewable: boolean = false;
  messageId: string = null;

  constructor(
    private userServices: UserService,
    private postServices: PostService,
    private httpService: HttpService,
    private authService: AuthService,
    ) { }

  ngOnInit() {
    try {
       this.user = this.userServices.user;
       this.getPosts();
    } catch (error) {
      this.userServices.getUser(AuthService.getUserId('userId'));
      this.userServices.userLoggedIn.subscribe(
        (result) => {
          this.data = result;
          this.user = this.data;
          this.getPosts();
        },
        err => console.warn(err)
      );
    }
  }

  getPosts() {
    
  }

  private datePretty(date: string): string {
     return UtilityService.formatDate(date);
  }

  private showMessage(id: string): void {
    this.messageId = id;
    this.messageViewable = !this.messageViewable;
  }

    updateUserHasSeen(id) {
    this.httpService.get(`/singleMessage/responder/${id}`, true, 'python').subscribe(
      (result) => {
      },
      err => console.warn(err)
    );
  }

}
