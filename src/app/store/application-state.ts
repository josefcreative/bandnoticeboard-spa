import { userSelectData } from "app/models/userSelect.models";
import { postGroupsData } from "app/store/reducers";
import { PostGroup } from "app/models/postGroup.models";
import * as fromRouter from '@ngrx/router-store';
import { Params } from "@angular/router";
import { User } from "../models/user.models";
import { Post } from "../models/post.models";

export interface PostTypesData {
    _id?: string;
    name?: string;
    postTypes?: Array<any>;
}

export interface PostGroupData {
    postGroups?: Array<any>;
    postTypesData?: PostTypesData;
    selectedPostType?: string;
    postGroupDisplay?: boolean;
    postTypeDisplay?: boolean;
}

export interface LocationData {
    place?: any;
    displayModal?: boolean;
}


export interface LoginCredentials {
    email: string;
    password: string;
}

export interface RouterStateUrl {
    url: string;
    queryParams: Params;
    params: any;
}

export interface PostDetails {
    offset: number;
    limit: number;
    total: number;
}

export interface PostData {
    posts: any;
    details: PostDetails;
}

export interface LoginData {
    message?: any,
    token?: string,
    userId?: string;
}

export interface UserData {
    loginData?: any;
    user?: User;
    error?: any;
    isLoggedIn: boolean;
}

export interface NewUserData {
    username: String;
    email: String;
    password: String;
    password2: String;
    acceptTerms: Boolean;
    city: any;
}

export interface reply {
    _id: String;
    userId: String;
    comment: String;
    date: Date;
    username: String;
}

export interface Replies {
    _id: String;
    userId: String;
    comment: String;
    authorId: String;
    authorSeen: String;
    date: Date;
    username: String;
    replySeen: String;
    replies: reply[]
}

export interface RepliesData {
    replies: Replies[]
}

export interface State {
    routerReducer?: fromRouter.RouterReducerState<RouterStateUrl>;
    userData: UserData;
    postData?: PostData;
    postGroupsData?: PostGroupData;
    locationData?: LocationData;
    newUserData?: NewUserData;
    singlePost: Post;
}

export const INITIAL_APPLICATION_STATE: State = {
    routerReducer: undefined,
    userData: {
        loginData: {},
        user: {},
        isLoggedIn: false,
    },
    postData: {
        posts: undefined,
        details: {
            offset: 0,
            limit: 12,
            total: 12,
        }
    },
    singlePost: undefined,
    locationData: {
        place: {},
        displayModal: false,
    },
    postGroupsData: {
        postGroups: [],
        postTypesData: {
            _id: undefined,
            name: undefined,
            postTypes: [],
        },
        selectedPostType: undefined,
        postGroupDisplay: undefined,
        postTypeDisplay: undefined,
    },
    newUserData: undefined,
};
