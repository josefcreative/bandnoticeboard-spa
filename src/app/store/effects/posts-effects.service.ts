
import {of as observableOf,  Observable } from 'rxjs';

import {withLatestFrom, map, catchError, switchMap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { PostsDataRecievedAction } from 'app/store/actions/actions';
import { postData } from 'app/store/reducers/postData';
import { Store, Action } from '@ngrx/store';
import { HttpService } from '../../services/http.service';
import { State } from '../application-state';
import {
  LOAD_POSTS_DATA,
  POST_TYPE_SELECTED,
  LOCATION_SELECTION,
  LOCATION_BY_USER_CITY,
  USER_DATA_RECIEVED
} from '../actions/actionTypes';

@Injectable()
export class PostsEffectsService {

  @Effect() initPosts$: Observable<Action> = this.actions$
    .ofType(LOAD_POSTS_DATA).pipe(
    switchMap(() => { 
          return this.http.get(`/posts/all/-1/0/12/`).pipe(
          map(posts => new PostsDataRecievedAction(posts)))
    }),
    catchError(err => observableOf(err)),)
    
  
  @Effect() postByPostType$: Observable<Action> = this.actions$
    .ofType(POST_TYPE_SELECTED).pipe(
    withLatestFrom(this.store$.select(state => state.postGroupsData.selectedPostType)),
    withLatestFrom(this.store$.select(state => state.locationData.place)),
    withLatestFrom(this.store$.select(state => state.postData.details)),
    switchMap(([[[action, selectedPostType], place], details]) => {
      return this.http.get(`/posts/all/-1/${details.offset}/${details.limit}/${place.place_id}/${selectedPostType}`).pipe(
        map(posts => new PostsDataRecievedAction(posts)));
    }),);

  @Effect() postByLocation$: Observable<Action> = this.actions$
    .ofType(LOCATION_SELECTION).pipe(
    withLatestFrom(this.store$.select(state => state.postGroupsData.selectedPostType)),
    withLatestFrom(this.store$.select(state => state.locationData.place)),
    withLatestFrom(this.store$.select(state => state.postData.details)),
    switchMap(([[[action, selectedPostType], place], details]) => {
      return this.http.get(`/posts/all/-1/${details.offset}/${details.limit}/${place.place_id}/${selectedPostType}`).pipe(
        map(posts => new PostsDataRecievedAction(posts)));
    }),);

    @Effect() postByUserLocation$: Observable<Action> = this.actions$
    .ofType(USER_DATA_RECIEVED).pipe(
    withLatestFrom(this.store$.select(state => state.postGroupsData.selectedPostType)),
    withLatestFrom(this.store$.select(state => state.userData.user.city)),
    withLatestFrom(this.store$.select(state => state.postData.details)),
    switchMap(([[[action, selectedPostType], city], details]) => {
      return this.http.get(`/posts/all/-1/${details.offset}/${details.limit}/${city.place_id}/${selectedPostType}`).pipe(
        map(posts => new PostsDataRecievedAction(posts)));
    }),);

  @Effect() postByParams$: Observable<any> = this.actions$
    .ofType('ROUTER_NAVIGATION').pipe(
    withLatestFrom(this.store$.select(state => state.postGroupsData.selectedPostType)),
    withLatestFrom(this.store$.select(state => state.locationData.place)),
    withLatestFrom(this.store$.select(state => state.postData.details)),
    withLatestFrom(this.store$.select(state => state.routerReducer)),
    switchMap(([[[[action, selectedPostType], place], details], router]) => {
      let routerOffset = router.state.params.paginate;
      if (typeof routerOffset === 'undefined') {
        this.store$.complete();
        return observableOf();
      }
      let offset = details.limit * routerOffset;
      return this.http.get(`/posts/all/-1/${offset}/${details.limit}/${place.place_id}/${selectedPostType}`).pipe(
        map(posts => new PostsDataRecievedAction(posts)));
    }),);
    

  constructor(
    private actions$: Actions,
    private postService: PostService,
    private http: HttpService,
    private store$: Store<State>,
  ) { 
  
  }
  
}
