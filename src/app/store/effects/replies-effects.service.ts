
import {withLatestFrom, map, switchMap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { LOAD_REPLIES_DATA } from '../actions/actionTypes';
import { HttpService } from '../../services/http.service';
import { State } from '../application-state';
import { RepliesDataRecieved } from '../actions/actions';


@Injectable()
export class RepliesEffectsService {

    @Effect() initReplies$: Observable<Action> = this.actions$
        .ofType(LOAD_REPLIES_DATA).pipe(
        withLatestFrom(this.store$.select(state => state.userData.user)),
        switchMap(([action, user]) => {
            return this.http.get(`/allmessages/${user._id}`, false, 'python').pipe(
                map(replies => new RepliesDataRecieved(replies)));
        }),)
        constructor(
            private actions$: Actions,
            private http: HttpService,
            private store$: Store<State>,
        ) {

        }
}