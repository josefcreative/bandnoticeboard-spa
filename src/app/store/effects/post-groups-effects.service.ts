
import {switchMap, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { PostGroupDataRecieved, RecievedPostTypes } from 'app/store/actions/actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { HttpService } from '../../services/http.service';
import { PostGroup } from 'app/models/postGroup.models';
import {
  LOAD_POST_GROUP,
  SELECTED_POST_TYPE_BY_ID,
} from '../actions/actionTypes';
@Injectable()
export class PostGroupsEffectsService {

  @Effect() PostGroup$: Observable<Action> = this.actions$
  .ofType(LOAD_POST_GROUP).pipe(
  switchMap(() => this.http.get(`/post-group`)),
  map((postGroupsData: PostGroup) => new PostGroupDataRecieved(postGroupsData)),);

  @Effect() PostTypes$: Observable<Action> = this.actions$
  .ofType(SELECTED_POST_TYPE_BY_ID).pipe(
  map(toPayload),
  switchMap((payload) => this.http.get(`/post-type/${payload}`)),
  map((postTypes: any[]) => new RecievedPostTypes(postTypes)),);
  
  constructor(
    private actions$: Actions,
    private http: HttpService,
  ) { }
}
