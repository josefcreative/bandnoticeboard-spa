
import {of as observableOf,  Observable } from 'rxjs';

import {switchMap, map, catchError} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';

import { State } from '../application-state';
import { HttpService } from '../../services/http.service';
import { NEW_USER_DATA_IN_STORE } from '../actions/actionTypes';
import { newUserData } from "../reducers";
import { NewUserLogin, LoginAttempt, LoginFailed } from "../actions/actions";

@Injectable()
export class SignUpEffectsService {

    @Effect() newUserDataCreated$: Observable<Action> = this.actions$
        .ofType(NEW_USER_DATA_IN_STORE).pipe(
        map(toPayload),
        switchMap(payload => {
            return this.http.post('/users', payload).pipe(
            map(() => {
                let userLoginData = {
                    email: payload.email,
                    password: payload.password,
                }
                return new LoginAttempt(userLoginData);
            }),
            catchError(err => observableOf(new LoginFailed(err))),);
        }),);

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private store$: Store<State>,
    ) {}

}
