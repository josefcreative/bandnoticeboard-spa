
import {of as observableOf,  Observable } from 'rxjs';

import {catchError, switchMap, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { HttpService } from '../../services/http.service';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  LoginAttempt,
  LoginSuccess,
  LoginFailed,
  UserDataRecieved,
  LoginFromStoredDetails,
  IsLoggedIn,
  LocationFromUserLogin,
} from '../actions/actions';
import {
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FROM_STORED_DETAILS,
} from '../actions/actionTypes';
import { LoginCredentials, State } from '../application-state';


@Injectable()
export class LoginEffectsService {

  @Effect() LoginAttempt$: Observable<Action> = this.actions$
    .ofType(LOGIN_ATTEMPT).pipe(
    map(toPayload),
    switchMap(payload => {
      let loginCredentials: LoginCredentials = payload;
       return this.http.login('/signin',loginCredentials).pipe(
      map(data => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('userId', data.userId);
        this.router.navigateByUrl('/posts/0');
        return new LoginSuccess(data);
      }),
      catchError(err => observableOf(new LoginFailed(err))),);
    }),);
  
  @Effect() getUserDetails$: Observable<Action> = this.actions$
    .ofType(LOGIN_SUCCESS).pipe(
    map(toPayload),
    switchMap(payload => {
      let token = localStorage.getItem('token');
      let id = payload.userId;
      this.store$.dispatch(new IsLoggedIn(true));
      return this.http.get(`/user/${id}`, true)
    }),
    map(data => {
     this.store$.dispatch(new LocationFromUserLogin(data.city));
      return new UserDataRecieved(data)
    }),
    catchError(err => observableOf(err)),)

  @Effect() LoginFromStoredDetails$: Observable<Action> = this.actions$
    .ofType(LOGIN_FROM_STORED_DETAILS).pipe(
    map(toPayload),
    switchMap(payload => {
      this.store$.dispatch(new IsLoggedIn(true));
      return this.http.get(`/user/${payload.userId}`, true)
    }),
    map(data => {
     this.store$.dispatch(new LocationFromUserLogin(data.city))
     this.store$.dispatch({type: 'LOCATION_BY_USER_CITY'})
      return new UserDataRecieved(data)
    }),
    catchError(err => observableOf(err)),)

  

  constructor(
    private actions$: Actions,
    private http: HttpService,
    private router: Router,
    private store$: Store<State>,
  ) { }

}
