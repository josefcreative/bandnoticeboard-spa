import { Action } from '@ngrx/store';

import {
    POSTS_DATA_RECIEVED,
    LOAD_POSTS_DATA,
    LOAD_POST_GROUP,
    POST_GROUP_DATA_RECIEVED,
    SELECTED_POST_TYPE_BY_ID,
    POST_TYPES_RECIEVED,
    POST_TYPE_SELECTED,
    DISPLAY_LOCATION_MODAL,
    HIDE_LOCATION_MODAL,
    LOCATION_SELECTION,
    LOGIN_ATTEMPT,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    USER_DATA_RECIEVED,
    LOGIN_FROM_STORED_DETAILS,
    IS_LOGGED_IN,
    LOGOUT_USER,
    LOCATION_FROM_USER_LOGIN,
    NEW_USER_DATA_IN_STORE,
    LOGIN_NEW_USER,
    LOAD_REPLIES_DATA,
    REPLIES_DATA_RECIVED,
} from './actionTypes';

import { Post } from '../../models/post.models';
import { PostGroup } from 'app/models/postGroup.models';
import { LoginCredentials, NewUserData, Replies } from '../application-state';
import { User } from '../../models/user.models';

export class PostsDataRecievedAction implements Action {
    readonly type = POSTS_DATA_RECIEVED;
    constructor(public payload?: any[]) {
    }
}

export class LoadPostsData implements Action {
    readonly type = LOAD_POSTS_DATA;
}

export class LoadPostGroups implements Action {
    readonly type = LOAD_POST_GROUP;
}

export class PostGroupDataRecieved implements Action {
    readonly type = POST_GROUP_DATA_RECIEVED;
    constructor(public payload?: PostGroup) {
    }
}

export class SelectedPostTypeById implements Action {
    readonly type = SELECTED_POST_TYPE_BY_ID;
    constructor(public payload: string) {
    }
}

export class RecievedPostTypes implements Action {
    readonly type = POST_TYPES_RECIEVED;
    constructor(public payload: any[]) {
    }
}

export class PostTypeSelected implements Action {
    readonly type = POST_TYPE_SELECTED;
    constructor(public payload: string){
    }
}

export class DisplayLocationModal implements Action {
    readonly type = DISPLAY_LOCATION_MODAL;
}

export class HideLocationModal implements Action {
    readonly type = HIDE_LOCATION_MODAL;
}

export class LocationSelection implements Action {
    readonly type = LOCATION_SELECTION;
    constructor(public payload: any) {
    }
}

export class LoginAttempt implements Action {
    readonly type = LOGIN_ATTEMPT;
    constructor(public payload: LoginCredentials){
    }
}

export class LoginSuccess implements Action {
    readonly type = LOGIN_SUCCESS;
    constructor(public payload: any){
    }
}

export class LoginFailed implements Action {
    readonly type = LOGIN_FAILED;
    constructor(public payload: any) {

    }
}

export class UserDataRecieved implements Action {
    readonly type = USER_DATA_RECIEVED;
    constructor(public payload: any) {
    }
}

export class LoginFromStoredDetails implements Action {
    readonly type = LOGIN_FROM_STORED_DETAILS;
    constructor(public payload: any) {
    }
}

export class IsLoggedIn implements Action {
    readonly type = IS_LOGGED_IN;
    constructor(public payload: any) {
    }
}

export class LogoutUser implements Action {
    readonly type = LOGOUT_USER;
    constructor(public payload: boolean) {
        localStorage.clear();
    }
}

export class LocationFromUserLogin implements Action {
    readonly type = LOCATION_FROM_USER_LOGIN;
    constructor(public payload: any) {
    }
}

export class NewUserDataInStore implements Action {
    readonly type = NEW_USER_DATA_IN_STORE;
    constructor(public payload: NewUserData) {
    }
}

export class NewUserLogin implements Action {
    readonly type = LOGIN_NEW_USER;
    constructor(public payload: NewUserData){
    }
}

export class LoadRepliesData implements Action {
    readonly type = LOAD_REPLIES_DATA;
}

export class RepliesDataRecieved implements Action {
    readonly type = REPLIES_DATA_RECIVED;
    constructor(public payload: Replies) {
        
    }
}