import { State } from "../application-state";
import {
    LOGIN_ATTEMPT,
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    USER_DATA_RECIEVED,
    IS_LOGGED_IN,
    LOGOUT_USER
} from "../actions/actionTypes";

export function userData(state: State, action: any) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            let loginData = action.payload;
            return {
                ...state,
                loginData,
            }
        case LOGIN_FAILED:
            let error = action.payload;
            return {
                ...state,
                error,
            }
        case USER_DATA_RECIEVED:
            let user = action.payload;
            return {
                ...state,
                user,
            }
        case IS_LOGGED_IN:
            let isLoggedIn = action.payload;
            return {
                ...state,
                isLoggedIn,
            }
        case LOGOUT_USER:
            isLoggedIn = action.payload;
            loginData = {};
            user = {};
            return {
                ...state,
                isLoggedIn,
                loginData,
                user,
            }
        default:
            return state;
    }
}