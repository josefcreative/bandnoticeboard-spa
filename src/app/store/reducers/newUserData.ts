import { State } from '../application-state';
import { NEW_USER_DATA_IN_STORE } from '../actions/actionTypes';

export function newUserData(state: State, action: any): State {
    switch(action.type) {
        case NEW_USER_DATA_IN_STORE:
            let newUserData = action.payload;
            return {
                ...state,
                ...newUserData,
            }
        default:
            return state;
    }
}