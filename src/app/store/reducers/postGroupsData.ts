import * as _ from 'lodash';

import { INITIAL_APPLICATION_STATE, State } from "app/store/application-state";
import {
    POST_GROUP_DATA_RECIEVED, POST_TYPES_RECIEVED, POST_TYPE_SELECTED,
} from "app/store/actions/actionTypes";

export function postGroupsData(state: State = INITIAL_APPLICATION_STATE, action: any) {
    switch (action.type) {
        case POST_GROUP_DATA_RECIEVED:
            let postGroups = _.values(action.payload); 
            return {
                ...state,
                postGroups,
            }
        case POST_TYPES_RECIEVED:
            return {
                ...state,
                postTypesData: action.payload,
            }
        case POST_TYPE_SELECTED:
            let selectedPostType = action.payload;
            return {
                ...state,
                selectedPostType,
            }
        default:
            return state;
    }
}