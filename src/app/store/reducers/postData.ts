import { State, INITIAL_APPLICATION_STATE } from "app/store/application-state";

import {
    POSTS_DATA_RECIEVED,
} from "../actions/actionTypes";
import { _catch } from "rxjs/operator/catch";

export function postData(state: State = INITIAL_APPLICATION_STATE, action: any): State {
    switch (action.type) {
        case POSTS_DATA_RECIEVED:
            let postData = action.payload
            return {
                ...state,
                ...postData,
            }
        default:
            return state;
    }
}
