import { postData } from './postData';
import { postGroupsData } from './postGroupsData';
import { locationData } from "./locationData";
import { userData } from './userData';
import { newUserData } from './newUserData';
import { repliesData } from './repliesData';

export {
    newUserData,
    userData,
    postData,
    postGroupsData,
    locationData,
    repliesData,
}