import { State } from "../application-state";
import {
    DISPLAY_LOCATION_MODAL,
    HIDE_LOCATION_MODAL,
    LOCATION_SELECTION,
    LOCATION_FROM_USER_LOGIN,
} from "../actions/actionTypes";

export function locationData(state: State, action: any) {
    switch(action.type) {
        case DISPLAY_LOCATION_MODAL:
            let displayModal = true;
            return {
                ...state,
                displayModal,
            }
        case HIDE_LOCATION_MODAL:
            displayModal = false;
            return {
                ...state,
                displayModal,
            }
        case LOCATION_SELECTION:
            let place = action.payload;
            return {
                ...state,
                place,
            }
        case LOCATION_FROM_USER_LOGIN:
            place = action.payload;
            return {
                ...state,
                place,
            }
        default:
            return state;
    }
}