
import {map} from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { Subject ,  Observable } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.models';
import { LoginCredentials, State } from '../../store/application-state';
import { Store } from '@ngrx/store';
import { LoginAttempt } from 'app/store/actions/actions';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  error$: Observable<Response>;
  loginForm: FormGroup;
  public userLogIn = new Subject();
  private LoginCredentials: LoginCredentials;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private store: Store<State>,
    ) { }

  ngOnInit() {
      this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required),
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.LoginCredentials = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    }
    this.store.dispatch(new LoginAttempt(this.LoginCredentials));
    this.error$ = this.store.pipe(map(this.matchFailedLoginToState))
  }

  private matchFailedLoginToState(state: State): Response {
    return state.userData.error;
  }

}
