import { NgModule } from '@angular/core';
import { Routes, RouterModule, Params, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import { AuthGuardService } from './services/auth-guard.service';

import { MainComponent } from './main/main.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { UserPostsComponent } from './user/user-posts/user-posts.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PostComponent } from './post/post.component';
import { SinglePostComponent } from './post/single-post/single-post.component';
import { TermsComponent } from './terms/terms.component';
import { MessagesComponent } from './messages/messages.component';
import { RouterStateUrl } from './store/application-state';

export class CustomSerializer implements fromRouter.RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        let route = routerState.root;
        while (route.firstChild) {
          route = route.firstChild;
        }
        const { url } = routerState;
        const queryParams = routerState.root.queryParams;
        const params = route.params;
        return { url, params, queryParams };
    }
}

const appRoutes: Routes = [
 { path: '', component: MainComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'user/:id', component: UserComponent },
  { path: 'user-posts/:id', component: UserPostsComponent, canActivate: [AuthGuardService] },
  { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuardService] },
  { path: 'contact', component: ContactUsComponent },
  { path: 'about', component: AboutUsComponent },
  { path: 'post', component: PostComponent },
  { path: 'post/:id', component: SinglePostComponent},
  { path: 'posts/:paginate', component: MainComponent},
  { path: 'terms-and-conditions', component: TermsComponent },
  { path: 'messages', component: MessagesComponent, canActivate: [AuthGuardService] },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {

}