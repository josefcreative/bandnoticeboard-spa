import { Component, OnInit, ViewChild,  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { HttpService } from '../services/http.service';
import { State, NewUserData } from '../store/application-state';
import { NewUserDataInStore } from '../store/actions/actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  private selectedLocation: any;
  private error: any;
  // private newUserData$: Observable<NewUserData>

  @ViewChild('f') signUpForm: NgForm;

  constructor(
    private httpService: HttpService,
    private router: Router,
    private store: Store<State>
  ) {

  }

  ngOnInit() {
  }

  getCityResult(city) {
    this.selectedLocation = city;
  }

  onSubmit(form: NgForm) {
    form.value.city = this.selectedLocation;
    this.store.dispatch(new NewUserDataInStore(form.value));
  }




}
