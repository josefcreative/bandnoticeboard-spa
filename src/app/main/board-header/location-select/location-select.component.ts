import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-location-select',
  template: `
    <div [ngClass]="showModal ? 'display' : 'hide'" class="location-container">
      <div class="modal">
      <div class="modal-dialog" role="document">
        <mat-card class="modal-content">
          <mat-card-header class="modal-header">
            <mat-card-title class="modal-title">Select / change a new location</mat-card-title>
            <button (click)="closeModal()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <mat-icon>close</mat-icon>
            </button>
          </mat-card-header>
          <div class="modal-body">
              <app-geo-location (cityResult)="getLocationResult($event)"></app-geo-location>
          </div>
          <div class="modal-footer" class="mat-typography">
            <button mat-raised-button color="accent" (click)="closeModal()" type="button" data-dismiss="modal">Close</button>
          </div>
        </mat-card>
      </div>
    </div>
    </div>
  `,
  styles: [`
    .display {
      display: block;
    }
    .hide {
      display: none;
    }
    .location-container {
      background: rgba(0, 0, 0, .8);
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    .modal {
      display: block!important;
    }
  `]
})
export class LocationSelectComponent implements OnInit {


  @Output() selectedLocation = new EventEmitter<any>();
  @Output() closeLocationModal = new EventEmitter<null>();
  @Input() showModal: boolean;

  constructor() { }

  ngOnInit() {

  }
  
  private closeModal(): void {
   this.closeLocationModal.emit();
  }

  public getLocationResult(location): void {
    this.selectedLocation.emit(location);
  }

}
