
import {skip, map} from 'rxjs/operators';
import { Component, OnInit, Input, EventEmitter, Output, Inject, OnChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { PostService } from './../../services/post.service';
import { Store } from '@ngrx/store';
import { State, PostTypesData, LocationData } from '../../store/application-state';
import { HttpService } from '../../services/http.service';


import {
  LoadPostsData,
  LoadPostGroups,
  SelectedPostTypeById,
  PostTypeSelected,
  DisplayLocationModal,
  HideLocationModal
} from 'app/store/actions/actions';
import { Observable } from 'rxjs';
import { PostGroup } from 'app/models/postGroup.models';
import { ILocation } from 'selenium-webdriver';
import { PostTypeDialogComponent } from '../post-type-dialog/post-type-dialog.component';

export interface IDialogData {
  postGroups$: Observable<PostGroup[]>
  postTypesData$: Observable<PostTypesData>;
  getPostGroupId: (T: Event) => void,
  getPostType: (T: Event) => void,
}

@Component({
  selector: 'app-board-header',
  templateUrl: './board-header.component.html',
  styleUrls: ['./board-header.component.scss']
})
export class BoardHeaderComponent implements OnInit {

  postGroups$: Observable<PostGroup[]>;
  postTypesData$:  Observable<PostTypesData>;
  locationData$: Observable<LocationData>;
  selectedPostGroup;
  selectedPostType;
  private displayLocationModal: boolean;
  refreshPostsSpin: boolean;
  postTypesHidden: boolean = false;
  postGroups: any;
  postTypes: any;

  constructor(
    private postService: PostService,
    private http: HttpService,
    private store: Store<State>,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.store.dispatch(new LoadPostGroups());
    this.postGroups$ = this.store.pipe(skip(1),map(this.mapStateToPostGroups),);
    this.postTypesData$ =  this.store.pipe(skip(1),map(this.mapStateToPostTypes));
    this.locationData$ = this.store.pipe(skip(1),map(this.mapStatetoLocationData),)

    this.postGroups$.subscribe(items => this.postGroups = items);
  }

  refreshPosts() {
    this.refreshPostsSpin = true;
    setTimeout(() => { // TODO create flag in store
      this.refreshPostsSpin = false;
    }, 1000);
    this.store.dispatch(new LoadPostsData());
  }

  mapStateToPostGroups(state: State): PostGroup[] {
    return state.postGroupsData.postGroups;
  }
  mapStateToPostTypes(state: State): PostTypesData {
    return state.postGroupsData.postTypesData;
  }

  mapStatetoLocationData(state: State): LocationData {
    return state.locationData;
  }

  getPostGroupId(id: string) {
    console.log('action')
    this.store.dispatch(new SelectedPostTypeById(id));
    this.postTypesHidden = true;
  }

  getPostType(postType: string): void {
    this.store.dispatch(new PostTypeSelected(postType));
    // http://localhost:4000/api/posts/post-types/Bassists Available
  }

  onClickLocationModal(): void {
    this.store.dispatch(new DisplayLocationModal());
  }


  openPostTypeDialog(): void {
    const dialogRef = this.dialog.open(PostTypeDialogComponent, {
      width: "350px",
      data: {
        postGroups: this.postGroups,
        postTypes$: this.postTypesData$,
        getPostGroupId: this.getPostGroupId.bind(this),
        getPostType: this.getPostType.bind(this),
      }
    });
  }


}
