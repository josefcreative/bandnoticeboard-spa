import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Response } from '@angular/http';
import { Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { User } from '../models/user.models';
import { MetaService } from '../services/meta.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  user: User;
  postDetails: any;
  postData: any;
  offset: number = 0;
  limit: number = 12;
  paginateNumber: number = 0;

  constructor(
  private authService: AuthService,
  private meta: Meta,
  private metaService: MetaService,
  private route: ActivatedRoute,
  ) {
   }

  ngOnInit() {
    // this.store
    // .dispatch(new LoadPostsData());

    // this.posts$ = this.store
    //                   .skip(1)
    //                   .map(this.mapStateToPosts);

    // this.store
    // .skip(1)
    // .subscribe(
    //   state => {
    //     this.postDetails = state['postData'].details;
    //     this.offset = state['postData'].details.offset;
    //   }
    // );

    this.metaService.mainMetaTags();

    
    // if (this.userService.user) {
    //   this.getPaginateWithRoute(this.userService.user);
    // } else {
    //   this.getPaginateWithRoute();
    //   this.userService.userLoggedIn.subscribe(
    //     (data: User) => {
    //       this.user = data;
    //       this.getPaginateWithRoute(this.user);
    //     },
    //     err => console.log(err)
    //   );
    // }
    // this.authService.logInStatus.subscribe(
    //   data => {
    //     if (data['status'] === 'LOGGED_OUT') {
    //       this.getPaginateWithRoute();
    //     }
    //   },
    //   err => console.warn(err)
    // );
  }

  ngOnDestroy () {
    this.metaService.removeMetaTags();
  }

  // getPaginateWithRoute(user: User = null) {
  //   return this.route.params.subscribe(
  //     params => {
  //       this.paginateNumber = params['paginate'] || 0;
  //       this.getQueryOffset(this.paginateNumber, user);
  //     }
  //   );
  // }

  // getQueryOffset(offset: number, user: User) {
  //   let place = '';
  //   if (user && user.city) {
  //     place = user.city.place_id;
  //   }
  //   this.offset = offset * this.limit;
  //   this.postService.getAllPosts(this.offset, this.limit, place);
  // }


  // mapStateToPosts(state: State): any[] {
  //   return state.postData.posts;
  // }

}
