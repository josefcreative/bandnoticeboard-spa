
import {skip, map} from 'rxjs/operators';
import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Response } from '@angular/http';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

import { HttpService } from '../../services/http.service';
import { Observable ,  ObservableInput } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from 'app/store/application-state';
import { HideLocationModal, LocationSelection, LoadPostsData } from 'app/store/actions/actions';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  posts$: Observable<any[]>;
  postDetails$: Observable<any>;
  showLocationModal$: Observable<boolean>;
  postDetails;
  offset;
  cardCols;

  constructor(
    private http: HttpService,
    private store: Store<State>,
    private breakpointObserver: BreakpointObserver
    ) {
    }



  // private watchUntilMobile() {
  //   if(window) {
  //     this.breakpointObserver.observe([
  //       Breakpoints.HandsetLandscape,
  //       Breakpoints.HandsetPortrait,
  //     ]).subscribe(result => {
  //       // result.matches ? this.colWidth = `100%` : this.colWidth = `33.3333%`;
  //       this.cardCols = 2;
  //     });
  //   }
  // }

  private watchUntilDesktop() {

  }

  private watchUntilDesktopWide() {

  }

  ngAfterViewInit() {
    // this.watchUntilMobile()
  }

  ngOnInit() {
    this.posts$ = this.store.pipe(skip(1),map(this.mapStateToPost),);
    this.postDetails$ = this.store.pipe(skip(1),map(this.mapStateToPostDetails),);
    this.showLocationModal$ = this.store.pipe(map(this.mapShowLocationModal));
      this.store.pipe(
      skip(1))
      .subscribe(
        state => {
          this.postDetails = state['postData'].details;
          this.offset = state['postData'].details.offset;
        }
      );
  }

  private locationClicked(): void {
    this.store.dispatch(new HideLocationModal());
  }

  private selectedNewLocation(location): void {
    this.store.dispatch(new LocationSelection(location));
  }

  // Map state to props methods
  private mapStateToPost(state: State): any[] {
    return state.postData.posts;
  }
  private mapStateToPostDetails(state: State): any {
    return state.postData.details;
  }
  private mapShowLocationModal(state: State): boolean {
    return state.locationData.displayModal;
  }
}
