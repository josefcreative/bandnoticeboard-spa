import { Component, OnInit, Input} from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { UtilityService } from '../../../services/utility.service';
import { Post } from '../../../models/post.models';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

@Input() private post: Post;

  constructor(private router: Router) { }

  ngOnInit() {

  }

  private datePretty(date: string): string {
     return UtilityService.formatDate(date);
  }


  private viewSelectedPost(e: Event, id: string): void {
    e.preventDefault();
    this.router.navigate([`/post/${id}`]);
  }

}
