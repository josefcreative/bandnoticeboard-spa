import { Component, OnInit, Inject, Input, OnChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { IDialogData } from "../board-header/board-header.component"
import { PostGroup } from 'app/models/postGroup.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-post-type-dialog',
  templateUrl: './post-type-dialog.component.html',
  styles: []
})
export class PostTypeDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PostTypeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData) { 
      
    }
  

  ngOnInit() {
  }

  ngOnChanges() {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }
}
