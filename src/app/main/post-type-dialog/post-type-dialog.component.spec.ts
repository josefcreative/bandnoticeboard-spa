import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostTypeDialogComponent } from './post-type-dialog.component';

describe('PostTypeDialogComponent', () => {
  let component: PostTypeDialogComponent;
  let fixture: ComponentFixture<PostTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostTypeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
