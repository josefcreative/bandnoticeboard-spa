import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { skip } from 'rxjs/operators';
import {  OnInit, DoCheck, OnDestroy } from '@angular/core';

import { AuthService } from '../services/auth.service';
import { HttpService } from '../services/http.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.models';
import { PostService } from '../services/post.service';
import { Store } from '@ngrx/store';
import { State, UserData } from '../store/application-state';
import { LoginFromStoredDetails, LogoutUser, LoadPostsData } from '../store/actions/actions';


@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent {

  private user: User;
  private result: any = {};
  private showUserDropDown: boolean = false;
  private userId: string;
  private localUserDetails: any;
  private hasSeenData: any;
  public hasNotSeen: boolean = false;
  private displayMenu: boolean = false;
  private _subscription;
  private user$: Observable<UserData>
  private isLoggedin: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private httpService: HttpService,
    private userService: UserService,
    private postService: PostService,
    private store: Store<State>,
    ) {}

    ngOnInit () {

      let localDetails = {
        token: localStorage.getItem('token'),
        userId: localStorage.getItem('userId'),
      }
      this.user$ = this.store.pipe(skip(1),map(this.mapUserToState),);
      this.user$
     .subscribe(
      userData => {
        this.isLoggedin = userData.isLoggedIn;
        if (!userData.isLoggedIn && localDetails.token && localDetails.userId) {
          this.store.dispatch(new LoginFromStoredDetails(localDetails));
        }
        if (userData) {
          this.user = userData.user;
          try {
            this.getHasSeenData(this.user._id);
          } catch (err) {
            this.userService.getUser(AuthService.getUserId('userId'));
            this.userService.userLoggedIn.subscribe(
             (user) => {
               this.user = user;
               this.user = this.userService.user;
               this.getHasSeenData(this.user._id);
             },
             error => console.log(error)
           );
          }
          this.authService.logInStatus.subscribe(
           data => {
             if (data['status'] === 'LOGGED_OUT') {
               this.userService.user = null;
             }
           },
           err => console.warn(err)
         );
        }
   
      }
     )
  
     if (!this.isLoggedin) {
      this.store.dispatch(new LoadPostsData());
     }
    }

    ngOnDestroy () {
      this._subscription.unsubscribe();
    }
  
    private showDropDown(): void {
      this.showUserDropDown = !this.showUserDropDown;
    }
  
    private getHasSeenData(id) {
      if (!id) {
        return
      }
      this.postService.haveNewPost(id);
      this.postService.shareHaveSeenData.subscribe(
        (result) => {
          this.hasSeenData = result;
          this.hasNewMessages();
        },
        err => console.log(err)
      );
    }

    private hasNewMessages(): void {
      let replies: any = null;
      try {
        const author: any = this.hasSeenData.author;
        for (let ii = 0; ii < this.hasSeenData.author.length; ii++) {
          for (let i = 0; i < this.hasSeenData.author[ii].replies.length; i++) {
              replies = this.hasSeenData.author[ii].replies;
              this.hasNotSeen = replies.some((hasSeen) => {
                return hasSeen.authorSeen === 'not_seen';
            });
          }
        }
      } catch (err) {
        console.log(`Couldn't get hasSeen Data - ${this.hasSeenData}`);
      }
    }
  
    private logOut() {
      this.store.dispatch(new LogoutUser(false));
      this.authService.logOut();
    }
  
  
    private showMenu() {
      this.displayMenu = !this.displayMenu;
    }
  
    mapUserToState(state: State): UserData {
      return state.userData;
    }

}
