export abstract class Post {
    constructor(
        public _id?: number,
        public userId?: string,
        public country?: string,
        public city?: any,
        public content?: string,
        public postGroup?: string,
        public updatedAt?: string,
        public postType?: string,
        public replies?: any[],
        public title?: string,
        public file?: any,
    ) {}
}
