export interface SelectionData {
  isLoggedIn?: boolean;
  userPlaceId?: string;
  postGroupType?: string;
  postType?: string;
  currentRoute?: string;
  currentPlaceId?: string; // This is for when there are no results returned then goto capital city
  userCapitalCityId?: string;
  currentCountry?: string; // should be country code ie 'GB'
}

export interface userSelectData {
  postGroupType?: string;
  postType?: string;
}
