export class User {
    constructor(
        public _id?: string,
        public email?: string,
        public dateJoined?: string,
        public acceptTerms?: boolean,
        public username?: string,
        public city?: any,
        public country?: string,
        public name?: string,
        public avatar?: string,
        public available?: string,
        public musicianType?: string,
        public posts?: [any],
        public messages?: [any],
        public isLoggedIn?: boolean,
        public error?: any,
    ) {}
}
