import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpService } from './../../services/http.service';
import { Post } from './../../models/post.models';
import { UtilityService } from './../../services/utility.service';
import { AuthService } from './../../services/auth.service';


@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.scss']
})
export class UserPostsComponent implements OnInit, OnDestroy {
  userId: number;
   sub: any;
   posts: Post[] = [];

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.userId = params['id'];
      this.getUserPosts();
    });
  }

  getUserPosts() {
    this.httpService.get(`/user-posts/${this.userId}`)
      .subscribe(
        (data) => {
          this.posts = data;
         },
        (error) => console.log(error)
      );
  }

   deletePost(id: string, userId: string): void {
    this.httpService.delete(`/posts/${userId}/${id}`)
      .subscribe(
        (data) => {
          this.getUserPosts();
        },
        error => console.log(error)
      );
  }

    datePretty(date) {
     return UtilityService.formatDate(date);
    }

  private viewPost(id: string): void {
    this.router.navigate([`/post/${id}`]);
  }

  ngOnDestroy() {
    //this.sub.subscribe();
  }

  updateAuthorHasSeen(id) {
    this.httpService.get(`/singleMessage/author/${id}`, true, 'python').subscribe(
      (result) => {
        console.log('SEEN POST', result)
      },
      err => console.log(err)
    );
  }

}
