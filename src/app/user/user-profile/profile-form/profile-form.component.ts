import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpService } from '../../../services/http.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.models';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Input() userData: User;
  @Input() showProfileForm: boolean = false;
  @Output() updateShowProfileForm = new EventEmitter<boolean>();
  @Output() updateUserData = new EventEmitter<User>();

  private updateProfileData: any;
  private profileUpdateForm: FormGroup;
  private changePassword = 'Edit';
  private result: any = {};

  constructor(private httpService: HttpService, private userService: UserService) { }

  ngOnInit() {
    this.profileUpdateForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'email': new FormControl(null, Validators.required),
      'password': new FormControl({value: null, disabled: true}, Validators.required),
      // 'musicianType': new FormControl('')
    });
    if (!this.userData) {
      this.userService.userLoggedIn.subscribe(
        (response) => {
          this.result = response;
          this.userData = this.result;
          this.setFormValues();
        },
        (error) => console.log(error)
      );
    } else {
      this.setFormValues();
    }
  }

    setFormValues() {
          this.profileUpdateForm.setValue({
      'username': this.userData.username || null,
      'email': this.userData.email || null,
      'password': null,
    });
  }

  private togglePassword(): void {
    if (this.profileUpdateForm.controls['password'].disabled) {
      this.changePassword = 'Unedit';
      this.profileUpdateForm.value.password = null;
      this.profileUpdateForm.controls['password'].enable();
    } else {
      this.changePassword = 'Edit';
      this.profileUpdateForm.controls['password'].disable();
    }
  }

  private toggleShowProfileForm(): void {
    this.showProfileForm = !this.showProfileForm;
    this.updateShowProfileForm.next(this.showProfileForm);
  }

    private getSelectedLocations(data) {

    // if (data.changeLocation && data.country.country && data.city) {
    //     this.updateProfileData.country = data.country.country;
    //     this.updateProfileData.city = data.city.name;
    // } else {
    //     this.updateProfileData.country = this.userData.country;
    //     this.updateProfileData.city = this.userData.city;
    // }
  }

  private postProfileForm(): void {
    this.updateProfile(this.profileUpdateForm.value);
  }

  private updateProfile(data) {
    this.httpService.put(`/users/${this.userData._id}`, data, true)
      .subscribe(
        (response) => {
          this.userData = response;
          this.updateUserData.next(this.userData);
          this.toggleShowProfileForm();
      },
        (error) => console.log(error)
      );
  }

}
