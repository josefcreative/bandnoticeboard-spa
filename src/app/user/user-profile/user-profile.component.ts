import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user.models';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
   user: User;
   result;
   showUserProfileForm = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
      this.user = this.userService.user;
      this.getUser();

  }

  showUserForm() {
    this.showUserProfileForm = !this.showUserProfileForm;
  }

  getShowProfile(value) {
    this.showUserProfileForm = value;
  }

  updateUserData(value) {
    this.user = value.user;
  }

  getUser() {
    this.userService.userLoggedIn.subscribe(
      (data) => {
        this.result = data;
        this.user = this.result;
      },
      error => console.log(error)
    );
  }
}
