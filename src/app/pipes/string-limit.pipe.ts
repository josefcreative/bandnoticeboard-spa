import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringLimit'
})
export class StringLimitPipe implements PipeTransform {

  transform(value: string, args: number): any {
    let limit = args
    let trail = args > 1 ? '...' : value;
    return value.length > limit ? value.substring(0, limit) + trail : value;
  }

}
