
import {map} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meta } from '@angular/platform-browser';
import { } from 'googlemaps';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State, UserData } from '../../store/application-state';

import { HttpService } from './../../services/http.service';
import { Post } from './../../models/post.models';
import { UtilityService } from './../../services/utility.service';
import { UserService } from './../../services/user.service';
import { User } from './../../models/user.models';
import * as _ from 'lodash';
import { MetaService } from './../../services/meta.service';
import { userData } from '../../store/reducers';
declare var google: any;

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit, OnDestroy {

  id: string;
  sub: any;
  post: Post;
  user: User;
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  private user$: Observable<User>;

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    private userService: UserService,
    private meta: Meta,
    private metaService: MetaService,
    private store: Store<State>,
  ) {
  }

  private createMeta(post: any): HTMLMetaElement[] {
   return this.meta.addTags([
     { name: 'description', content: _.truncate(post.content, {'length': 150, 'omission': ''}) },
     { property: 'og:type', content: 'website' },
     { name: 'keywords', content: 'Musicians,Guitarists,Bassists,Drummers,Singers,Vocalists,Saxophonists,Pianists,Keyboardists,Djs'},
     { property: 'og:url', content: `http://www.bandnoticeboard.com/post/${post._id}` },
     { property: 'og:site_name', content: 'Bandnoticeboard.com' },
     { property: 'og:title', content: post.title },
     { property: 'og:country-name', content: post.city.address_components[post.city.address_components.length - 1].long_name},
     { property: 'og:description', content: _.truncate(post.content, {'length': 150, 'omission': ''}) },
     { property: 'og:region', content: post.city.name },
     { property: 'og:locality', content: post.city.vicinity },
     { property: 'og:image',
     content: Array.isArray(post.image) && post.image.length > 0 && post.image[0].url ? post.image[0].url : 'http://via.placeholder.com/600X600' },
    { property: 'og:image:type',
    content: Array.isArray(post.image) && post.image.length > 0 && post.image[0].resource_type ? `${post.image[0].resource_type}/${post.image[0].format}` : '' },
    { name: 'twitter:card', content: 'summary' },
    { name: 'twitter:site', content: '@bandnoticeboard' },
    { name: 'twitter:title', content: `${post.title} | in ${post.city}, ${post.country} | Bandnoticeboard` },
    { name: 'twitter:image',
    content: Array.isArray(post.image) && post.image.length > 0 && post.image[0].url ? post.image[0].url : '' },
    { name: 'twitter:url', content: `http://www.bandnoticeboard.com/post/${post._id}` },
    { name: 'twitter:description', content: _.truncate(post.content, {'length': 150, 'omission': ''}) }
    ]);
  }

  ngOnInit() {
    this.user$ = this.store.pipe(map(this.mapUserToState));
    this.user$
      .subscribe(
        user => {
          this.user = user;
        }
      )

    this.sub = this.route.params.subscribe(
      (params) => {
        this.id = params['id'];
        this.getPost(this.id);
      }
      );
  }

  mapUserToState(state: State): User {
    return state.userData.user;
  }

  getPost(id) {
    this.httpService.get(`/post/${id}`)
      .subscribe(
        (data) => {
          data.updatedAt = UtilityService.formatDate(data.updatedAt);
          this.post = data;
          this.createMeta(this.post);
          // ---------------- google maps
          var mapProp = {
            center: new google.maps.LatLng(this.post.city.geometry.location.lat, this.post.city.geometry.location.lng),
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
        },
        (error) => console.log(error)
      ); 
  }

  ngOnDestroy() {
    this.metaService.removeMetaTags();
    this.sub.unsubscribe();
  }

}
