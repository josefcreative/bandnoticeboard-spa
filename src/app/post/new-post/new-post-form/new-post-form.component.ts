
import {map} from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State, UserData } from '../../../store/application-state';

import { HttpService } from '../../../services/http.service';
import { PostGroup } from '../../../models/postGroup.models';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.models';
import { Post } from '../../../models/post.models';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-post-form',
  templateUrl: './new-post-form.component.html',
  styleUrls: ['./new-post-form.component.scss']
})
export class NewPostFormComponent implements OnInit {

  private token = localStorage.getItem('token');
  private postGroups: PostGroup[] = [];
  private postTypes: string[] = [];
  private newPostForm: FormGroup;
  private postTypesLength: number;
  private user: User = null;
  private result: any = {};
  private isSubmited: boolean = false;
  private showForm: boolean = true;
  private showError: boolean = false;
  private errorMsg: any = {};
  private aggrPostData: Post = {};
  private selectedLocation: any = null;
  private showCityInput: boolean = true;
  private user$: Observable<UserData>

  constructor(
    private httpService: HttpService,
    private userService: UserService,
    private route: ActivatedRoute,
    private store: Store<State>,
  ) {
      this.postTypes = null;
  }

  ngOnInit() {
    this.getPostGroup();
    this.user$ = this.store.pipe(map(this.mapUserToState))
    this.user$
      .subscribe(
        userData => {
          this.user = userData.user;
        }
      )

    this.newPostForm = new FormGroup({
      'title': new FormControl('', Validators.required),
      'postGroup': new FormControl(null, Validators.required),
      'postType': new FormControl(null, Validators.required),
      'content': new FormControl(null, Validators.required),
      'file': new FormControl(null)
    });
  }

  mapUserToState(state: State): UserData {
    return state.userData;
  }

  getCityResult(city) {
    this.selectedLocation = city;
  }

  showCitySelection() {
    this.showCityInput=!this.showCityInput;
  }

  private fileChange(event) {
    let fileList: FileList = event.srcElement.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      this.aggrPostData.file = file;
    }
  }

  private onSubmit(): void {
    const formData = new FormData();
    this.isSubmited = true;
    formData.append('title', this.newPostForm.value.title);
    formData.append('content', this.newPostForm.value.content);
    formData.append('postGroup', this.newPostForm.value.postGroup.name);
    formData.append('postType', this.newPostForm.value.postType.name);
    formData.append('file', this.aggrPostData.file);
    if (!this.selectedLocation) {
      let placeId = this.user.city.place_id; 
        formData.append('city', placeId);
    } else {
      formData.append('city', this.selectedLocation.place_id);
    }
    let xhr: XMLHttpRequest = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", (ev: ProgressEvent) => {
    });
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          this.showForm = false;
          this.isSubmited = false;
        } else {
          this.showError = true;
          this.isSubmited = false;
          this.errorMsg = xhr.response;
        }
      }
    };
    const localUrl = 'http://localhost:4000/api/posts/';
    const url = 'http://138.68.153.125:4000/api/posts/';

    xhr.open("POST", `${localUrl}${this.user._id}/${this.token}`, true);
    xhr.send(formData);
  }

  private postTypesActive(): boolean {
    return this.postTypes != null;
  }

  private getPostGroup(): void {
    this.httpService.get('/post-group')
      .subscribe(
      (data) => {
        this.postGroups = data;
      },
      (error) => console.log(error),
    );
  }

  private getPostTypes(): void {
    const postGroupId = this.newPostForm.value.postGroup._id;
    this.httpService.get(`/post-type/${postGroupId}`)
      .subscribe(
      (data) => {
        this.postTypes = data.postTypes;
        this.postTypesLength = this.postTypes.length - 1;
      },
      (error) => console.log(error),
    );
  }

  ngOnDestroy() {
  }

}
