import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule  } from '@angular/common/http';
import { StoreModule, ActionReducer, combineReducers, ActionReducerMap } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { Action } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatChipsModule} from '@angular/material/chips';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BoardComponent } from './main/board/board.component';
import { HeroComponent } from './hero/hero.component';
import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './main/board/card/card.component';
import { SignupComponent } from './signup/signup.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { UserComponent } from './user/user.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { BoardHeaderComponent } from './main/board-header/board-header.component';
import { PostComponent } from './post/post.component';
import { NewPostComponent } from './post/new-post/new-post.component';
import { NewPostFormComponent } from './post/new-post/new-post-form/new-post-form.component';
import { SinglePostComponent } from './post/single-post/single-post.component';
import { LocationSelectComponent } from './main/board-header/location-select/location-select.component';
import { ProfileFormComponent } from './user/user-profile/profile-form/profile-form.component';
import { LocationSelectionComponent } from './directives/location-selection/location-selection.component';
import { PaginateComponent } from './directives/paginate/paginate.component';
import { ReplyFormComponent } from './directives/reply-form/reply-form.component';
import { MessagesComponent } from './messages/messages.component';
import { PostRepliesComponent } from './directives/post-replies/post-replies.component';
import { GeoLocationComponent } from './directives/geo-location/geo-location.component';
import { ScriptsComponent } from './scripts/scripts.component';

// Services
import { HttpService } from './services/http.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { PostService } from './services/post.service';
import { AuthGuardService } from './services/auth-guard.service';
import { MetaService } from './services/meta.service';
import { LocationService } from './services/location.service';
import { UserSelectService } from './services/user-select.service';

// Routing
import { AppRoutingModule, CustomSerializer } from './app-routing.module';


// Directives
import { BoardDropDownComponent } from './directives/board-drop-down/board-drop-down.component';
import { PreLoaderComponent } from './directives/pre-loader/pre-loader.component';
import { ProfileDetailComponent } from './directives/profile-detail/profile-detail.component';
import { UserPostsComponent } from './user/user-posts/user-posts.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { TermsComponent } from './terms/terms.component';

// Pipes
import { StringLimitPipe } from './pipes/string-limit.pipe';

// Cloudinary
import { Cloudinary } from 'cloudinary-core/cloudinary-core-shrinkwrap';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
const cloudinaryLib = {
  Cloudinary: Cloudinary
};

// STORE

import { storeFreeze } from 'ngrx-store-freeze';
export const metaReducers = [storeFreeze];

import { INITIAL_APPLICATION_STATE, State } from './store/application-state';
import { 
  userData,
  postData,
  postGroupsData,
  locationData,
  newUserData,
  repliesData,
} from './store/reducers';

import { PostsEffectsService } from './store/effects/posts-effects.service';
import { PostGroupsEffectsService } from './store/effects/post-groups-effects.service';
import { LoginEffectsService } from './store/effects/login-effects.service';
import { SignUpEffectsService } from './store/effects/signup-effects.service';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule, MatIconModule } from '@angular/material';
import {MatListModule} from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import { MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { PostTypeDialogComponent } from './main/post-type-dialog/post-type-dialog.component';
import {MatBadgeModule} from '@angular/material/badge';

const reducers = {
  routerReducer: fromRouter.routerReducer,
  userData,
  postData,
  postGroupsData,
  locationData,
  newUserData,
  repliesData,
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BoardComponent,
    HeroComponent,
    FooterComponent,
    CardComponent,
    SignupComponent,
    MainComponent,
    LoginComponent,
    LoginFormComponent,
    UserComponent,
    AboutUsComponent,
    ContactUsComponent,
    BoardHeaderComponent,
    BoardDropDownComponent,
    PostComponent,
    NewPostComponent,
    NewPostFormComponent,
    SinglePostComponent,
    PreLoaderComponent,
    LocationSelectComponent,
    ProfileDetailComponent,
    UserPostsComponent,
    UserProfileComponent,
    TermsComponent,
    StringLimitPipe,
    ProfileFormComponent,
    LocationSelectionComponent,
    PaginateComponent,
    ReplyFormComponent,
    MessagesComponent,
    PostRepliesComponent,
    GeoLocationComponent,
    ScriptsComponent,
    MainNavComponent,
    PostTypeDialogComponent,
  ],
  entryComponents: [
    PostTypeDialogComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'bandNoticeboard-seo' }),
    StoreModule.forRoot(reducers, {metaReducers, initialState: INITIAL_APPLICATION_STATE}),
    EffectsModule.forRoot(
      [
        PostsEffectsService,
        PostGroupsEffectsService,
        LoginEffectsService,
        SignUpEffectsService,
      ]
    ),
    StoreRouterConnectingModule,
    StoreDevtoolsModule.instrument(),
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    CloudinaryModule.forRoot(cloudinaryLib, { cloud_name: 'josefcreative'}),
    BrowserAnimationsModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatChipsModule,
    MatCardModule,
    MatGridListModule,
    FlexLayoutModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatBadgeModule,
  ],
  providers: [
    HttpService,
    AuthService,
    UserService,
    PostService,
    AuthGuardService,
    MetaService,
    LocationService,
    UserSelectService,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
