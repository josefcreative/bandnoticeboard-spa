import { Component, OnInit, Input } from '@angular/core';

import { Post } from '../../models/post.models';
import { UtilityService } from '../../services/utility.service';
import { User } from '../../models/user.models';

@Component({
  selector: 'app-post-replies',
  template: `
  <div class="card card-outline-secondary mb-3">
    <div class="card-block">
      <blockquote class="card-blockquote">
        <div *ngFor="let message of post?.replies">
          <strong>{{message?.username}}</strong>
          <p>{{message?.comment}}</p>
          <footer><i class="fa fa-calendar" aria-hidden="true"></i>  <cite>{{datePretty(message?.date)}}</cite></footer>
          <cite>Replies</cite> <i class="fa fa-level-down" aria-hidden="true"></i>
          <hr>
          <blockquote class="blockquote" *ngFor="let msg of message?.replies">
            <strong>{{msg?.username}}</strong>
            <p>{{msg?.comment}}</p>
            <footer><i class="fa fa-calendar" aria-hidden="true"></i>  <cite>{{datePretty(msg?.date)}}</cite></footer>
          </blockquote>
          <app-reply-form 
          [hasSeenType]="hasSeenType"
          [messageId]="message[idType]"
          [messagePath]="'singleMessage'"
          [post]="post"
          [user]="user"
          ></app-reply-form>
        </div>
      </blockquote>
    </div>
  </div>
  `,
  styles: [`
  .card {
    margin-top: 20px;
  }
  `]
})
export class PostRepliesComponent implements OnInit {

  @Input() post: Post;
  @Input() hasSeenType: string;
  @Input() idType = Symbol();
  @Input() user: User;

  constructor() { }

  ngOnInit() {
  }

  private datePretty(date: string): string {
     return UtilityService.formatDate(date);
  }

}
