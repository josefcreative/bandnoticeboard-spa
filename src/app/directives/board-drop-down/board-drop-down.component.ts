import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-board-drop-down',
  template: `
        <mat-form-field class="mat-typography">
          <mat-select placeholder="{{name}}" class="mat-typography">
              <mat-option *ngFor="let listItem of listItems"
              [value]="listItem[property]"
              (click)="selectListItem(listItem[props])">
              {{listItem[property]}}
              </mat-option>
            </mat-select>
          </mat-form-field>
  `,
  styles: [`
    .display-menu {
      display: block;
    }
    .hide-menu {
      display-none;
    }
    mat-form-field {
      padding: 6px 20px;
      margin-left: 10px;
    }
    @media screen and (max-width: 768px) {
        margin-bottom: 10px;
    }
  `]
})
export class BoardDropDownComponent implements OnInit {

  @Input() listItems;
  @Input() btn: string;
  @Input() name: string;
  @Input() property: string;
  @Input() props: any;

  @Output() selectedListItem = new EventEmitter<string>();

  isShown: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  dropDown() {
    this.isShown = !this.isShown;
  }

  selectListItem(selectedItem: string): void {
    this.selectedListItem.emit(selectedItem);
  }
}
