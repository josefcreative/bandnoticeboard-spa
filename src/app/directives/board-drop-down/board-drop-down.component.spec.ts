import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDropDownComponent } from './board-drop-down.component';

describe('BoardDropDownComponent', () => {
  let component: BoardDropDownComponent;
  let fixture: ComponentFixture<BoardDropDownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardDropDownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
