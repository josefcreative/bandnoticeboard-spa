import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import * as _ from 'lodash';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-geo-location',
  template: `
  <form>
      <label>Enter your local town / city:</label>
      <mat-form-field class="geo-form-field">
      <input
      placeholder="your city / town"
      matInput
      autocomplete="off"
      (input)="getCity($event)"
      [(ngModel)]="selectedCityDescription"
      id="city"
      name="city"
      required
      >
    </mat-form-field>

    <mat-list *ngIf="cities.length > 0" class="mat-typography">
        <mat-list-item *ngFor="let city of cities" class="mat-typography">
          <span (click)="selectCity(city)">{{city.description}}</span>
        </mat-list-item>
        <mat-divider></mat-divider>
    </mat-list>
  </form>
  `,
  styles: [`
  .geo-form-field {
    width: 100%;
  }
  `]
})

export class GeoLocationComponent implements OnInit {

  @Output() cityResult = new EventEmitter<any>();

  private cities: any = [];
  private city: any;
  private selectedCity: any = {};
  private selectedCityDescription: string;

  constructor(private httpService: HttpService) { }

  ngOnInit() {

  }

  getCity($event) {
    let city = $event.target.value;
    this.httpService.get(`/cities/${city}`)
      .subscribe(
      data => {
        this.cities = data.predictions;
      },
      error => console.log(error)
      );
  }


  selectCity(city) {
    this.selectedCity = city;
    this.selectedCityDescription = city.description;
    this.cityResult.emit(this.selectedCity);
    this.cities = [];
  }


}
