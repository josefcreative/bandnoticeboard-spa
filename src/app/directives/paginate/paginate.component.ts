import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { PostDetails } from '../../store/application-state';

@Component({
  selector: 'app-paginate',
  template: `
          <div class="container d-flex justify-content-center">
            <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li *ngFor="let page of pageArray" class="page-item">
                <a class="page-link" [routerLink]="'/posts/'+ page">{{ page + 1 }}</a>
              </li>
            </ul>
            </nav>
          </div>
  `,
  styles: [],
})
export class PaginateComponent implements OnInit {
  @Input() paginateData$: Observable<PostDetails>;
  private totalPages: number;
  private limit: number;
  private offset: number;
  private pageArray: number[] = [];
  private paginateData: any;

  constructor() { }

  ngOnInit() {
    this.paginateData$.subscribe(
      data => {
        this.paginateData = data;
        this.totalPages = this.paginateData.count;
        this.limit = this.paginateData.limit;
        this.offset = this.paginateData.offset;
        this.createPagination(this.totalPages, this.limit);
      },
      err => console.log(err)
    )
  }

  ngOnchanges() {
  }

  private createPagination(total: number, limit: number): void {
    this.pageArray = [];
    const perPage: number = total / limit;
    for (let i = 0; i < perPage; i++) {
      this.pageArray.push(i);
    }
  }
}