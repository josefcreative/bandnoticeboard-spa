import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-location-selection',
  templateUrl: './location-selection.component.html',
  styles: [`
  `]
})
export class LocationSelectionComponent implements OnInit {
  private disabled: boolean = true;
  private storedCountry = null;
  private storedCity = null;
  @Input() private userCountry: string;
  @Input() private userCity: string;
  private locations: any[] = [];
  @Output() locationSelection = new EventEmitter<any>();
  private getLocations: Subject<any> = new Subject();
  private locationForm: FormGroup;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.getUserLocations();
      this.locationForm = new FormGroup({
      'country': new FormControl({ value: null }, Validators.required),
      'city': new FormControl({ value: null }, Validators.required),
      'changeLocation': new FormControl(false),
    });
  }

  private enableLocation() {
    this.disabled = !this.disabled;
    if (!this.locationForm.value.changeLocation) {
      this.storedCountry = this.userCountry;
      this.storedCity = this.userCity;
    } else {
      this.getCountries();
      this.getLocations.subscribe(
        (data) => {
          this.storedCountry = null;
          this.storedCity = null;
          if (this.locations.length > 0) {
            return;
          };
          this.locations = data;
        },
        (error) => console.log(error)
      );
    }
  }

  private locationResults() {
    this.locationSelection.emit(this.locationForm.value);
  }

  private getCountries() {
    this.httpService.get('/locations')
      .subscribe(
      (data) => {
        this.getLocations.next(data);
      },
      (error) => console.log(error)
      );
  }

  private getUserLocations() {
     this.storedCountry = this.userCountry;
      this.storedCity = this.userCity;
  }

}
