import { Component, OnInit, Input, ElementRef, OnChanges } from '@angular/core';
import { NgForm } from '@angular/forms';

import { HttpService } from '../../services/http.service';
import { User } from '../../models/user.models';
import { Post } from '../../models/post.models';
import { UserService } from './../../services/user.service';

@Component({
  selector: 'app-reply-form',
  template: `
<div *ngIf=senderUser?._id>
  <div class="container reply-form">
  <div [ngClass]="successMsg ? 'display-none' : 'display-block'">
    <button [ngClass]="showReplyForm ? 'display-none' : 'display-block'"
    (click)="replyToAdd()" type="button" class="btn btn-success btn-lg btn-block">Reply</button>
    <div [ngClass]="showReplyForm ? 'display-block' : 'display-none'" class="well">
        <form (ngSubmit)="submitReplyMessage(f)" #f="ngForm">
          <div class="form-group">
            <label for="exampleTextarea">Message:</label>
            <textarea
            class="form-control"
            id="exampleTextarea" rows="7"
            ngModel
            name="message"
            required></textarea>
          </div>
          <button [disabled]="!f.touched && !f.valid" type="submit" class="btn btn-info">Submit</button>
          <button (click)="replyToAdd(); showReplyForm ? 'display-none' : 'display-block'"
          type="button" class="btn btn-danger">Cancel</button>
        </form>     
    </div>
  </div>
  <div *ngIf="successMsg" class="alert alert-success" role="alert">
    <strong>{{successMsg}}</strong>
  </div>
  </div>
</div>
  `,
  styles: [
    `
    .reply-form {
      margin-bottom: 20px;
    }
    `
  ]
})
export class ReplyFormComponent implements OnInit {

  @Input() messagePath: string;
  @Input() post: Post;
  @Input() messageId: number;
  @Input() hasSeenType: string = '';
  @Input() user: User;
  isUser: boolean;
  senderUser: User;
  showReplyForm: boolean;
  formData: any = {};
  successMsg: string;
  PYTHON_PATH: string;

  constructor(private httpService: HttpService, private userService: UserService) { }

  ngOnInit() {
    this.senderUser = this.user;
  }

  replyToAdd() {
    this.senderUser = this.user;
    this.showReplyForm = !this.showReplyForm;
  }

  submitReplyMessage(messageData: NgForm) {
    this.formData = {
      authorId: this.post.userId,
      userId: this.senderUser._id,
      username: this.senderUser.username,
      comment: messageData.value.message,
    };
    this.updateUserHasSeen();
    this.httpService.post(`/${this.messagePath}${this.hasSeenType}/${this.messageId}`, this.formData, true, 'python').subscribe(
      (result) => {
        this.successMsg = result.success;
      },
      error => console.log(error)
    );
  }

  updateUserHasSeen() {
    this.httpService.get(`/${this.messagePath}${this.hasSeenType}/${this.messageId}`, true, 'python').subscribe(
      (result) => {
      },
      err => console.warn(err)
    );
  }

  ngOnchanges() {
    this.senderUser = this.user;
  }

  ngOnDestroy() {
    this.senderUser = this.user;
  }


}
