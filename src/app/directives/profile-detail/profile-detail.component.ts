import { Component, OnInit, OnChanges, Input, AfterContentChecked } from '@angular/core';

import { UserService } from './../../services/user.service';
import { User } from './../../models/user.models';
import { UtilityService } from './../../services/utility.service';
import { PostService } from './../../services/post.service';

@Component({
  selector: 'app-profile-detail',
  template: `
        <div *ngIf="user?._id">
            <div class="d-flex flex-column justify-content-center">
             <h2 class="pull-left"><i class="pull-left fa fa-user-o" aria-hidden="true"></i> {{author?.username}}</h2>
              <small *ngIf="author?.musicianType">Musician type: <span class="badge badge-info">{{author?.musicianType}}</span></small>
             <small *ngIf="author?.country">Location: <span class="badge badge-primary">{{author?.country}} | {{author?.city}}
             </span></small>
            </div>
        </div>
  `,
  styles: [`
    h2 {
      font-weight: 200;
    }
    i {
      color: #91CED7;
    }
    .badge {
      padding: 5px 5px;
      font-weight: 300;
    }
  `]
})
export class ProfileDetailComponent implements OnInit {
  @Input() authorId: string;
  @Input() user: User;
   author: User;
   result: any;
   sub: any;

  constructor(private userService: UserService, private postServices: PostService) { }

  ngOnInit() {
  }

  private getUser() {
     this.postServices.sharePostUser.subscribe(
        (data) => {
          this.result = data;
          this.result.dateJoined = UtilityService.formatDate(this.result.dateJoined);
          this.author = this.result;
        },
        error => console.log(error)
     );
  }

  ngOnChanges() {
    console.log('here -----', this.user)
    this.getUser();
    if (!this.user) {
      return;
    } else if (this.authorId && typeof this.author === 'undefined') {
        this.postServices.getPostUser(this.authorId);
      }
    }

}
