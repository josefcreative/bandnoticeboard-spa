import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pre-loader',
  template: `
  <div
  *ngIf="wait"
  [style.backgroundColor]="bgColor"
  [style.color]="preLoaderColor"
  class="pre-loader--main d-flex justify-content-center align-items-center"
  >
      <div><i class="fa fa-refresh fa-spin fa-4x fa-fw"></i></div>
  </div>
  `,
  styles: [`
    .pre-loader--main {
      position: absolute;
      z-index: 3;
      width: 100%;
      height: 100%;
      top: 0;
    }
  `]
})
export class PreLoaderComponent implements OnInit {

  @Input() bgColor;
  @Input() preLoaderColor;
  @Input() wait;

  constructor() {

   }

  ngOnInit() {
  }

}
