import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class UtilityService {

  public static formatDate(date) {
    if (!date) {
      return;
    }
    try {
      return moment(date).format('llll');
    } catch (error) {
        console.warn('Date is not correct format: ', error);
    }
  }

  constructor() { }

}
