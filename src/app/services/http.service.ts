
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { NgForm } from '@angular/forms';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import { Credentials } from '../models/credentials.models';

@Injectable()
export class HttpService {

URL: String;
// NODE_URL: String = 'http://localhost:4000/api';
  NODE_URL: String = 'http://68.183.39.200:4000/api';
// PYTHON_URL: String = 'http://localhost:5000';
 PYTHON_URL: String = 'http://68.183.39.200:8001';

headers = new Headers({ 'Content-Type': 'application/json'});
options: any = new RequestOptions({ headers: this.headers});
token: string;

  constructor(private http: Http) {}

  public get(noun: String, token: boolean = false, api: string = 'node'): Observable<any> {
      api === 'python' ? this.URL = this.PYTHON_URL : this.URL = this.NODE_URL;
      if (token) {
        this.token = localStorage.getItem('token');
        return this.http.get(`${this.URL}${noun}/${this.token}`).pipe(
          map((response: Response) => response.json()));
      } else {
        return this.http.get(`${this.URL}${noun}`).pipe(
          map((response: Response) => response.json()));
      }
    }

    post(noun: String, data: any, token: boolean = false, api: string = 'node', options = this.options): Observable<any> {
      api === 'python' ? this.URL = this.PYTHON_URL : this.URL = this.NODE_URL;
      const body: any = JSON.stringify(data);
      if (token) {
        this.token = localStorage.getItem('token');
        return this.http.post(`${this.URL}${noun}/${this.token}`, body, options).pipe(
        map((response: Response) => response.json()));
      } else {
        return this.http.post(`${this.URL}${noun}`, body, options).pipe(
        map((response: Response) => response.json()));
      }
    }

    put(noun: String, data: any, token: boolean = false, api: string = 'node'): Observable<any> {
      api === 'python' ? this.URL = this.PYTHON_URL : this.URL = this.NODE_URL;
      const body: any = JSON.stringify(data);
      if (token) {
        this.token = localStorage.getItem('token');
        return this.http.put(`${this.URL}${noun}/${this.token}`, body, this.options).pipe(
          map((response: Response) => response.json()));
      }
    }

    delete(noun: String): Observable<any> {
      this.URL = this.NODE_URL;
      this.token = localStorage.getItem('token');
        return this.http.delete(`${this.URL}${noun}/${this.token}`, this.options).pipe(
        map((response: Response) => response.json()));
    }

    login(noun: String, data: Credentials): Observable<any> {
      this.URL = this.NODE_URL;
      const body: any = JSON.stringify(data);
      return this.http.post(`${this.URL}${noun}`, body, this.options).pipe(
        map((response: Response) => response.json()));
    }

}
