import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject ,  Observable ,  Observer } from 'rxjs';

import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { User } from 'app/models/user.models';


interface SelectionData  {
  isLoggedIn?: boolean;
  userPlaceId?: string;
  postGroupType?: string;
  postType?: string;
  currentRoute?: string;
  currentPlaceId?: string; // This is for when there are no results returned then goto capital city
  userCapitalCityId?: string;
  currentCountry?: string; // should be country code ie 'GB'
}

interface userSelectData {
  postGroupType?: string;
  postType?: string;
}

@Injectable()
export class UserSelectService {

  private _selectionData: SelectionData = {};
  private user: User;
  private userSelectedPostType = new Subject();
  private userLogInStatus = new Subject();

  public updateSelectionData = Observable.create((observer: Observer<SelectionData>) => {
    this.route.params.subscribe(
      params => {
       this._selectionData.currentRoute = params['paginate'];
       observer.next(this.selectionData);
      },
      error => observer.error(error)
    );
    this.userSelectedPostType.subscribe(
      types => {
        this._selectionData.postGroupType = types['postGroupType'];
        this._selectionData.postType = types['postType'];
        observer.next(this.selectionData);
      },
      err => observer.error(err) 
    );
    this.authService.logInStatus.subscribe(
      data => {
        if (data['status'] === 'LOGGED_OUT') {
          this._selectionData.isLoggedIn = data['status'];
          observer.next(this.selectionData);
        }
      },
      err => observer.error(err)
    );
    this.userService.userLoggedIn.subscribe(
      (user: User) => {
        this._selectionData.userPlaceId = user.city.place_id;
        observer.next(this.selectionData);
      },
      err => observer.error(err)
    );
  });

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService,
  ) {
  
  }

  public get selectionData() {
    return this._selectionData;
  }

  public postTypeSelection(options: userSelectData = {}): void {
    let _options = options || {};
    _options.postGroupType = options.postGroupType || '';
    _options.postType = options.postType || '';
    this.userSelectedPostType.next(_options);
  }


  private listenToRouteParams() {
    this.route.params.subscribe(
      params => {
       this._selectionData.currentRoute = params['paginate'];
      }
    );
  }

  private listenToUserLoggedInStatus() {

  }



}
