
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observer, NextObserver ,  Observable } from 'rxjs';


interface Location {
  lat: number;
  lng: number;
}

interface GeoLocation {
  location: Location;
}

interface GeoLocationInfo {
  country?: string;
  cities?: [any];
  city?: string;
  lat?: number;
  lng?: number;
}

@Injectable()
export class LocationService {

  private geoLocation_API: string = 'https://www.googleapis.com/geolocation/v1/geolocate?key=';
  private geoLocation_KEY: string = 'AIzaSyB4j3SKY27k7gIDTBc2NsG-dBsLHCxH7fg';
  private getNames_API: string = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';
  private getNames_KEY: string = 'band_noticeboard';
  public geoLocationInfo: GeoLocationInfo = {};

  private getGeoLocationData = Observable.create((observer: NextObserver<GeoLocation>) => {
    this.getGeoLocation()
      .subscribe(
        data => {
          this.geoLocationInfo.lat = data.location.lat;
          this.geoLocationInfo.lng = data.location.lng;
          observer.next(data);
          observer.complete();
        },
        error => observer.error(error)
      );
  });

  public getLocation = Observable.create((observer: Observer<any>) => {
      this.getGeoLocationData.subscribe(
        data => {
          this.getClosestCity(this.geoLocationInfo.lat, this.geoLocationInfo.lng)
            .subscribe(
              data => {
               observer.next(data);
               observer.complete();
              },
              error => observer.error(error)
            );
        },
        error => observer.error(error)
      );
  });

  constructor(private http: Http) {
  }

  private getGeoLocation(): Observable<GeoLocation> {
    return this.http.post(`${this.geoLocation_API}${this.geoLocation_KEY}`, null).pipe(
    map((response: Response) => response.json()));
  }

  private getClosestCity(lat, lng): Observable<any> {
    return this.http.post(`${this.getNames_API}location=${lat},${lng}&radius=20&type=cities&key=${this.geoLocation_KEY}`, null).pipe(
      map((response: Response) => response.json()));
  }

}
