import { Injectable } from '@angular/core';

import { HttpService } from './http.service';
import { Credentials } from '../models/credentials.models';
import { UserService } from '../services/user.service';
import { Subject } from 'rxjs';

@Injectable()
export class AuthService {

  token: string;
  userData: any = {};
  public logInStatus = new Subject();

  public static isAuthenticated(token: any): boolean {
    return token != null;
  }
  public static getUserId(item: string): string {
    return localStorage.getItem(item);
  }

  constructor(private http: HttpService, private userService: UserService) {}

  public setToken(data: any): void {
    localStorage.setItem('token', data.token);
    localStorage.setItem('userId', data.userId);
  }


  public logOut(): void {
    this.logInStatus.next({ status: 'LOGGED_OUT'});
  }

  public isLoggedOn(): boolean {
    return localStorage.getItem('token') !== null;
  }

  public getUser() :void {
    
  }


}
