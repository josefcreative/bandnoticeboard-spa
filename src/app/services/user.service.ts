import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { User } from '../models/user.models';
import { HttpService } from '../services/http.service';

@Injectable()
export class UserService {

  public user: User;
  public userLoggedIn = new Subject();

  constructor(private httpService: HttpService) { }

  getUser(id: string) {
    if (!id) {
      return;
    }
    this.httpService.get(`/user/${id}`, true)
      .subscribe(
        (data: User) => {
          this.user = data;
          this.userLoggedIn.next(this.user);
        },
        (error) => console.error(error)
      );
  }

}
