import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable()
export class MetaService {

  constructor(private meta: Meta) { }


  public removeMetaTags(): void {
    this.meta.removeTag("name='description'");
    this.meta.removeTag("name='keywords'");
    this.meta.removeTag("property='og:type'");
    this.meta.removeTag("property='og:url'");
    this.meta.removeTag("property='og:site_name'");
    this.meta.removeTag("property='og:title'");
    this.meta.removeTag("property='og:country-name'");
    this.meta.removeTag("property='og:description'");
    this.meta.removeTag("property='og:region'");
    this.meta.removeTag("property='og:locality'");
    this.meta.removeTag("property='og:image'");
    this.meta.removeTag("property='og:image:type'");
    this.meta.removeTag("name='twitter:card'");
    this.meta.removeTag("name='twitter:site'");
    this.meta.removeTag("name='twitter:card'");
    this.meta.removeTag("name='twitter:image'");
    this.meta.removeTag("name='twitter:card'");
    this.meta.removeTag("name='twitter:url'");
    this.meta.removeTag("name='twitter:description'");
  }

  public mainMetaTags(): HTMLMetaElement[] {
    return this.meta.addTags([
      { name: 'description',
      content:'Fed up of wasting time, searching countless sites for a gig? Checkout Band Noticeboard today & become part of a growing community or like minded musicians.' },
      { name: 'keywords', content: 'Musicians,Guitarists,Bassists,Drummers,Singers,Vocalists,Saxophonists,Pianists,Keyboardists,Djs'},
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'http://www.bandnoticeboard.com/' },
      { property: 'og:site_name', content: 'Bandnoticeboard.com' },
      { property: 'og:title', content: 'Welcome to Band Noticeboard' },
      { property: 'og:country-name', content: 'United Kingdom' },
      { property: 'og:description',
      content: 'Fed up of wasting time, searching countless sites for a gig? Checkout Band Noticeboard today & become part of a growing community or like minded musicians.' },
      { property: 'og:region', content: 'London' },
      { property: 'og:locality', content: 'London' },
      { property: 'og:image',
      content: 'http://res.cloudinary.com/josefcreative/image/upload/v1497309011/wesley-tingey-223059_mmpb30.jpg' },
     { property: 'og:image:type',
     content: 'image/jpg' },
     { name: 'twitter:card', content: 'summary' },
     { name: 'twitter:site', content: '@bandnoticeboard' },
     { name: 'twitter:title', content: `Welcome to Band Noticeboard | in London, United Kingdom | Bandnoticeboard` },
     { name: 'twitter:image',
     content: 'http://res.cloudinary.com/josefcreative/image/upload/v1497309011/wesley-tingey-223059_mmpb30.jpg' },
     { name: 'twitter:url', content: `http://www.bandnoticeboard.com/` },
     { name: 'twitter:description', content: 'Fed up of wasting time, searching countless sites for a gig? Checkout Band Noticeboard today & become part of a growing community or like minded musicians.' }
    ]);
  }

}
