import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Post } from '../models/post.models';
import { HttpService } from '../services/http.service';
import { User } from '../models/user.models';


@Injectable()
export class PostService {

  public postData: Post[] = [];
  public sharePosts = new Subject();
  public postUser: User;
  public sharePostUser = new Subject();
  public hasSeenData: any;
  public shareHaveSeenData = new Subject();


  constructor(private httpService: HttpService) {}

  getAllPosts(offset: number = 0, limit: number = 0, place: string = '') {
    this.httpService.get(`/posts/all/-1/${offset}/${limit}/${place}`) ///api/posts/all/:sort/:offset/:limit
      .subscribe(
         (data) => {
             this.postData = data;
             this.sharePosts.next(this.postData); 
         },
      );
  }


  getPostUser(id: string) {
    if (!id) {
      return;
    }
    this.httpService.get(`/user/${id}`, true)
      .subscribe(
        (data) => {
          this.postUser = data;
          this.sharePostUser.next(this.postUser);
        },
        (error) => console.error(error)
      );
  }

  haveNewPost(id: string): void {
    this.httpService.get(`/hasmessages/${id}`, true, 'python').subscribe(
      (result) => {
        this.hasSeenData = result;
        this.shareHaveSeenData.next(this.hasSeenData);
      },
      err => console.warn('Python @message service: ', err)
    );
  }
}
