#!/bin/bash
echo Started script to deploy Angular app...
# stop server
[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh 
nvm use 10
forever stop server.js
# these get pulled in from the repo even if you forget to build the anguler project
rm -rf bandnoticeboard-spa/
# Remove all files exept for the pakage.json & deploy.sh
find . -type f -not -name 'package.json' -not -name 'deploy.sh' -not -name 'server.js' -not -name 'sitemap.xml'  -not -name 'node_modules' -delete

# Remove Git
rm -rf .git

# Reclone from repo
git clone https://josefcreative@bitbucket.org/josefcreative/bandnoticeboard-spa.git && echo cloned
echo Git repo cloned successfully...
# pull files out of dir the
cd bandnoticeboard-spa/
# get the dist dir
if [ -d dist ]; then
    echo Working with dist files...
    mv dist ..
    cd ..
    rm -rf bandnoticeboard-spa/
    cd dist/
    mv * .[^.]* ..
    cd ..
    rm -rf dist/
    # restart the server
    rm -rf node_modules/
    npm install
    forever start server.js
    echo deployment SUCCESS!
else
    echo No Dist Directory, you need to run ng build on your AngularJS project.
fi